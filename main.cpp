#include <iostream>
#include <cassert>
#include "matrix.hpp"

using saw::Vec2d;
using saw::Mat22d;

int main() {
	std::cout << "=== Test 1 ===" << std::endl;
	{
		Mat22d A({ {
			 {1,2},
			 {3,4}
		} });

		Vec2d X({ {
			{1},
			{1}
		} });

		auto B = A * X;

		assert(B.get(0, 0) == 3);
		assert(B.get(1, 0) == 7);
	}
	std::cout << "Done!" << std::endl;

	std::cout << "=== Test 2 ===" << std::endl;
	{
		Mat22d A({ {
			 {1,1},
			 {1,1}
		} });

		Mat22d X({ {
			{1,5},
			{1,3}
		} });

		auto B = A + X;
		assert(B.get(0, 0) == 2);
		assert(B.get(0, 1) == 6);
		assert(B.get(1, 0) == 2);
		assert(B.get(1, 1) == 4);
	}
	std::cout << "Done!" << std::endl;

	std::cout << "=== Test 3 ===" << std::endl;
	{
		Mat22d A({ {
			 {5,3},
			 {4,7}
		} });

		Mat22d X({ {
			{1,5},
			{1,3}
		} });

		auto B = A - X;
		assert(B.get(0, 0) == 4);
		assert(B.get(0, 1) == -2);
		assert(B.get(1, 0) == 3);
		assert(B.get(1, 1) == 4);
	}
	std::cout << "Done!" << std::endl;


	std::cout << "=== Test 4 ===" << std::endl;
	{
		Mat22d A({ {
			 {5,3},
			 {4,7}
		} });

		auto B = A.transpos();
		assert(B.get(0, 0) == 5);
		assert(B.get(0, 1) == 4);
		assert(B.get(1, 0) == 3);
		assert(B.get(1, 1) == 7);
	}
	std::cout << "Done!" << std::endl;
	
	std::cout << "=== Test 5 ===" << std::endl;
	{
		Mat22d A({ {
			 {5,3},
			 {4,7}
		} });
		auto det = A.determinat();
		assert(det == 23);
	}
	std::cout << "Done!" << std::endl;
	
	std::cout << "=== Test 6 ===" << std::endl;
	{
		Mat22d A({ {
			 {5,2},
			 {17,7}
		} });

		auto B = A.inv();
		assert(B.get(0, 0) == 7);
		assert(B.get(0, 1) == -2);
		assert(B.get(1, 0) == -17);
		assert(B.get(1, 1) == 5);
	}

	return 0;
}